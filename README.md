# Simple Todo Application

### Tech used:

* JavaScript + ES6
* ReactJS
* jQuery
* NodeJS with Express library

### Demo ([click])


   [click]: <https://brook-todo-list.herokuapp.com>
