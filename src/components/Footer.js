import React from 'react';

export default ({filter, handleFilter, remaining}) => (
  <div className="info flex__row">
    <span className="remaining">{remaining} remaining</span>
    <div className="filters flex__row">
      <button
        className={filter == 'all' ? 'selected' : ''}
        onClick={() => handleFilter('all')}>All</button>
      <button
        className={filter == 'done' ? 'selected' : ''}
        onClick={() => handleFilter('done')}>Done</button>
      <button
        className={filter == 'remaining' ? 'selected' : ''}
        onClick={() => handleFilter('remaining')}>Remaining</button>
    </div>
  </div>
);
