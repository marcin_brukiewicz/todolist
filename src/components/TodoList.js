import React from 'react';
import {
  TransitionMotion,
  spring,
  presets
} from 'react-motion';
export default class TodoList extends React.Component {
  getDefaultStyles() {
    console.log('Getting default styles!');
    return this.props.todos.map(todo => ({...todo, style: {height: 0, opacity: 1}}));
  };
  getStyles() {
    console.log('Getting styles!');
    const { inputValue, todos, filter } = this.props;
    return todos
      .filter(({data: { done, name }}) => {
        return name.toLowerCase().includes(inputValue.toLowerCase()) &&
        (filter === 'done' && done ||
        filter === 'remaining' && !done ||
        filter === 'all');
      })
      .map(todo => {
        return {
          ...todo,
          style: {
            height: spring(60, presets.gentle),
            opacity: spring(1, presets.gentle),
          }
        }
      });
  }
  willEnter() {
    return { height: 0, opacity: 1 };
  }
  willLeave() {
    return { height: spring(0), opacity: spring(0) };
  }

  render() {
    const { handleToggle, handleRemove } = this.props;
    return (
      <div className="todos">
        <TransitionMotion
          defaultStyles={this.getDefaultStyles()}
          styles={this.getStyles()}
          willLeave={this.willLeave}
          willEnter={this.willEnter}>
          {styles =>
            <ul className="todo-list">
              {styles.map(({key, style, data: {done, name}}) =>
                <li key={key} style={style} className={done ? 'done' : ''}>
                  <div className="view flex__row cen-ver">
                    <input type="checkbox" className="toggle" onChange={() => handleToggle(key)}/>
                    <label>{name}</label>
                    <button className="destroy" onClick={() => handleRemove(key)}></button>
                  </div>
                </li>
              )}
            </ul>
          }
        </TransitionMotion>
      </div>
    )
  }
}
