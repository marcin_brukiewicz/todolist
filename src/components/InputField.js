import React from 'react';

export default ({handleSubmit, inputValue, handleChange}) => (
  <form onSubmit={handleSubmit}>
    <input
      type="text"
      value={inputValue}
      autoFocus
      placeholder="Add your new todo item!"
      onChange={handleChange}
    />
  </form>
);
