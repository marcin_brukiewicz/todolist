import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/todos';
import TodoList from '../components/TodoList';
import InputField from '../components/InputField';
import Footer from '../components/Footer';

class TodosContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { inputValue: '' }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.handleRemove = this.handleRemove.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
  }
  handleChange(event) {
    this.setState({
      inputValue: event.target.value
    });
  }
  handleSubmit(e) {
    e.preventDefault();
    this.props.addTodo(this.state.inputValue);
  }
  handleToggle(id) {
    this.props.toggleTodo(id);
  }
  handleRemove(id) {
    this.props.removeTodo(id);
  }
  handleFilter(filter) {
    console.log('Filter', this, this.props);
    this.props.filterTodos(filter);
  }

  render() {
    const { inputValue } = this.state;
    const { todos, filter } = this.props;
    console.log('Todos:', todos, filter);
    return (
      <div className="wrapper">
        <h1 className="text-center">Your todo list</h1>
        <div className="todos__container flex__column">
          <InputField
            handleSubmit={this.handleSubmit}
            handleChange={this.handleChange}
            inputValue={inputValue}
          />
          <TodoList
            handleToggle={this.handleToggle}
            handleRemove={this.handleRemove}
            inputValue={inputValue}
            todos={todos}
            filter={filter}
          />
          <Footer
            filter={filter}
            remaining={todos.filter(x => !x.data.done).length}
            handleFilter={this.handleFilter}
          />

        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  todos: state.app.todos.map(todo => {
    return {
      key: todo.id + "",
      data: { name: todo.name, done: todo.done }
    }
  }),
  filter: state.app.filter
});

export default connect(mapStateToProps, actions)(TodosContainer);
