export const todosList = [
  {
    id: 0,
    name: 'Write a new app',
    done: false
  },
  {
    id: 1,
    name: 'Exercise',
    done: false
  },
  {
    id: 2,
    name: 'Learn how to animate apps',
    done: false
  },
  {
    id: 3,
    name: 'Drive granny home',
    done: false
  },
  {
    id: 4,
    name: 'Chill outside',
    done: false
  }
];
