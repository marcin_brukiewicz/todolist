export const addTodo = (name) => (dispatch, getState) => {
  dispatch({
    type: 'ADD_TODO',
    todo: {
      id: getState().app.todos.pop().id + 1,
      name,
      done: false
    }
  });
};

export const filterTodos = (filter) => (dispatch, getState) => {
  dispatch({
    type: 'FILTER_TODOS',
    filter
  });
}
export const removeTodo = (id) => (dispatch, getState) => {
  dispatch({
    type: 'REMOVE_TODO',
    id
  });
};

export const toggleTodo = (id) => (dispatch, getState) => {
  dispatch({
    type: 'TOGGLE_TODO',
    id
  });
};
