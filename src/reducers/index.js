import { combineReducers } from 'redux';
import app from './todos';

const rootReducer = combineReducers({
  app
});

export default rootReducer;
