import { todosList } from '../services/preloadedDB';
import { combineReducers } from 'redux';

const todos = (state = todosList, action) => {
  switch (action.type) {
    case 'ADD_TODO': {
      return [
        ...state,
        action.todo
      ];
    }
    case 'REMOVE_TODO': {
      return state.filter(todo => todo.id != action.id);
    }
    case 'TOGGLE_TODO': {
      return state.map(todo => action.id == todo.id ? { ...todo, done: !todo.done } : todo);
    }
    default:
      return state;
  }
}

const filter = (state = 'all', action) => {
  switch (action.type) {
    case 'FILTER_TODOS':
      return action.filter;
    default:
      return state;
  }
}


export default combineReducers({
  todos,
  filter
});
