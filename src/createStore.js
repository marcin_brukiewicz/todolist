import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import rootReducer from './reducers';
import thunk from 'redux-thunk'

export default () => {
  const middlewares = [];
  if (process.env.NODE_ENV == 'production') {
    middlewares.push(logger);
  }
  middlewares.push(thunk);

  return createStore(
    rootReducer,
    applyMiddleware(...middlewares)
  );
}
